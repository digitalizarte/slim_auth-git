$(document).ready(function () {
    $('#abmEmpresaModal').on('show.bs.modal', function (evt) {
        var $this = $(this);
        if (evt.relatedTarget) {
            $('#abmEmpresaModalLabel').text('Alta Empresa');
            $this.find('[name="empresaid"]').val('');
            $this.find('[name="nombre"]').prop('readonly', false).val('');
            $this.find('[name="formadepago"]').val('');
        }

        $this.find('[autofocus]:not([readonly]):first').focus();
    });
    $('#btnEditarEmpresa').on('click', function () {
        var empresaid = $('#cboEmpresaid').val();
        $.get(basepath + '/admin/empresa/' + empresaid, function (data) {
            $('#abmEmpresaModalLabel').text('Editar Empresa');
            var $modal = $('#abmEmpresaModal'),
                obj = JSON.parse(data)
            for (var item in obj) {
                var $aux = $modal.find('#' + item.toLowerCase());
                if ($aux.length) {
                    $aux.val(obj[ item]);
                }
            }
            $('#hdnEmpresaId').val(empresaid);
            $modal.find('[name="nombre"]').prop('readonly', true);
            $modal.modal('show');
        });
    });
    $('button.agregar-opcion').on('click', function (evt) {
        evt.preventDefault();
        $('.opcion-pago:not(:visible):first').show();
    });
    $('.opcion-pago button.close').on('click', function (evt) {
        evt.preventDefault();
        $(this).parents('.row:first').hide().find(':input').each(function () {
            $(this).val('');
        });
    });
});