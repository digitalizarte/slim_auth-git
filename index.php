<?php

session_start();

/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
require_once 'Slim/Slim.php';
define('ROOT_PATH', '');
define('REDIRECT_ROOT_PATH', '');
\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\HaangaView(),
    'log.enabled' => true,
    'debug' => true
        ));

$authFn = function( $role, $redirect = null ) {
    if (!is_array($role)) {
        $role = array($role);
    }

    return function() use ($role, $redirect) {
        if (!(( isset($_SESSION['login.level']) && in_array($_SESSION['login.level'], $role) ) || in_array('*', $role))) {
            if (is_null($redirect) || $app->request()->isAjax()) {
                $app = \Slim\Slim::getInstance();
                $app->halt(401, '401 Unauthorized');
            } else {
                $app->redirect(REDIRECT_ROOT_PATH . $redirect);
            }
        }
    };
};

$authAll = $authFn('*');
$authAdmin = $authFn('admin');
$authUser = $authFn('user');

/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, `Slim::patch`, and `Slim::delete`
 * is an anonymous function.
 */
// Routes
$app->get(ROOT_PATH . '/', $authAll, 'getRoot');
$app->post(ROOT_PATH . '/', $authAll, 'postRoot');
$app->get(ROOT_PATH . '/admin', $authAdmin, 'getAdmin');
$app->get(ROOT_PATH . '/admin/', $authAdmin, 'getAdmin');
$app->get(ROOT_PATH . '/user', $authUser, 'getUser');
$app->get(ROOT_PATH . '/user/', $authUser, 'getUser');

function getRoot() {
    $app = \Slim\Slim::getInstance();
    $data = array('basepath' => REDIRECT_ROOT_PATH, 'user' => null, 'pwd' => null, 'message' => null);
    $app->render('index.tpl.html', $data);
}

function postRoot() {

    $app = \Slim\Slim::getInstance();
    $user = $app->request->post('user');
    $pwd = $app->request->post('pwd');
    $data = array('basepath' => REDIRECT_ROOT_PATH, 'user' => $user, 'pwd' => $pwd, 'message' => '');
    $userNOK = empty($user);
    $pwdNOK = empty($pwd);

    if ($userNOK) {
        $data['message'] .= 'Please comple username.';
    }

    if ($pwdNOK) {
        $data['message'] .= 'Please comple password.';
    }

    unset($_SESSION['login.level']);
    if (!($userNOK || $pwdNOK)) {
        switch ($user) {
            case 'admin':
                if ($pwd == 'admin') {
                    $_SESSION['login.level'] = 'admin';
                    $app->redirect('/admin');
                } else {
                    $data['message'] .= 'Username or Password incorrect.';
                }

                break;

            case 'user':
                if ($pwd == 'user') {
                    $_SESSION['login.level'] = 'user';
                    $app->redirect('/user');
                } else {
                    $data['message'] .= 'Username or Password incorrect.';
                }

                break;

            default:
                $data['message'] .= 'Username or Password incorrect.';
                break;
        }
    }

    $app->render('index.tpl.html', $data);
}

function getAdmin() {
    $app = \Slim\Slim::getInstance();
    $app->render('admin.tpl.html', array('basepath' => REDIRECT_ROOT_PATH));
}

function getUser() {
    $app = \Slim\Slim::getInstance();
    $app->render('user.tpl.html', array('basepath' => REDIRECT_ROOT_PATH));
}

/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();
